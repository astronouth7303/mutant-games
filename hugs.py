#!/usr/bin/env python3
"""
Select your monster by clicking on it.

Click and hold the mouse to move the monster.

Try to hug all the people by getting close to them.
"""
import ppb
from ppb.events import ReplaceScene
import random
from ppb_mutant import Emoji
from utils import (
    CircularRegion, CircularMenuScene, AnimationSprite, MenuSprite,
    clamp,
)


print(__doc__)


class MotionMixin:
    target: ppb.Vector = None
    velocity: ppb.Vector = None
    max_speed: float = None

    def on_update(self, update, signal):
        if self.target is not None:
            if self.max_speed is None:
                self.position = self.target
            else:
                delta = (self.target - self.position)
                # Calculate the maximum amount we could travel in this time, and
                # limit to that.
                delta = delta.truncate(self.max_speed * update.time_delta)
                self.position += delta
        elif self.velocity is not None:
            v = self.velocity
            if self.max_speed is not None:
                v = v.truncate(self.max_speed)
            self.position += v * update.time_delta

        camera = update.scene.main_camera
        self.position = ppb.Vector(
            clamp(camera.frame_left, self.position.x, camera.frame_right),
            clamp(camera.frame_bottom, self.position.y, camera.frame_top),
        )


def sign(x):
    from math import copysign
    return copysign(1, x)


class RunnerSprite(MotionMixin, CircularRegion, ppb.Sprite):
    scared: bool = False
    scare_distance: float = 1.5
    relax_distance: float = 1.5 * scare_distance

    image_scared = Emoji('scared')
    image_worried = Emoji('worried')

    @property
    def image(self):
        if self.scared:
            return self.image_scared
        else:
            return self.image_worried

    @property
    def max_speed(self):
        scared_speed = 1
        if self.scared:
            return scared_speed
        else:
            return 0.9 * scared_speed

    def __init__(self, *p, **kw):
        super().__init__(*p, **kw)
        self.velocity = ppb.Vector(0, 1).rotate(
            random.uniform(-180, 180)
        )

    def _check_bear(self, bear):
        # Use a Schmitt trigger to avoid oscillating behaviour
        d = (self.position - bear.position).length
        if d < self.scare_distance:
            self.scared = True
        elif d > self.relax_distance:
            self.scared = False

    def _check_walls(self, camera):
        # TODO: Do something gentler
        if not (camera.frame_left < self.position.x < camera.frame_right):
            if sign(camera.position.x - self.position.x) != sign(self.velocity.x):
                self.velocity = self.velocity.reflect(ppb.Vector(1, 0))

        if not (camera.frame_bottom < self.position.y < camera.frame_top):
            if sign(camera.position.y - self.position.y) != sign(self.velocity.y):
                self.velocity = self.velocity.reflect(ppb.Vector(0, 1))

    def _direction(self, bear, time_delta):
        max_angle = 360 * time_delta  # Max. 1 turn per second
        rot = random.triangular(-max_angle, +max_angle)
        if self.scared:
            d = self.position - bear.position
            rot += self.velocity.angle(d)
            rot = clamp(-max_angle, rot, max_angle)

        self.velocity = self.velocity.rotate(rot).scale(self.max_speed)

    def on_update(self, update, signal):
        self._check_bear(update.scene.player)
        self._direction(update.scene.player, update.time_delta)
        self._check_walls(update.scene.main_camera)

        super().on_update(update, signal)


class HuggedSprite(ppb.Sprite):
    image = Emoji('smile_hearts')


class HeartAnimSprite(AnimationSprite):
    image = Emoji('red_heart')

    line = ppb.Vector(0, 1)
    duration = 1

    def do_start(self, signal):
        self.position = self.aposition

    def do_frame(self, dt, t, signal):
        self.position = self.line * (t / self.duration) + self.aposition
        return t < self.duration


class PlayerSprite(MotionMixin, CircularRegion, ppb.Sprite):
    max_speed = 1.5

    target = None
    velocity = ppb.Vector(0, 0)

    def on_button_pressed(self, mouse, signal):
        if mouse.button is ppb.buttons.Primary:
            self.target = mouse.position

    def on_button_released(self, mouse, signal):
        if mouse.button is ppb.buttons.Primary:
            self.target = None

    def on_mouse_motion(self, mouse, signal):
        if ppb.buttons.Primary in mouse.buttons:
            self.target = mouse.position

    def on_update(self, update, signal):
        from math import tanh
        super().on_update(update, signal)

        weight = sum(
            1 / (runner.position - self.position).length
            for runner in update.scene.get(kind=RunnerSprite)
            if runner.scared
        )

        base_speed = 1.5
        real_speed = 3
        self.max_speed = base_speed + (real_speed - base_speed) * tanh(weight)


class MainScene(ppb.BaseScene):
    runner_count = 10

    player_image = None

    background_color = (0, 100, 0)

    def __init__(self, *p, player_image, **kw):
        super().__init__(*p, player_image=player_image, **kw)

        for _ in range(self.runner_count):
            self.add(RunnerSprite(pos=(
                random.uniform(self.main_camera.frame_left, self.main_camera.frame_right),
                random.uniform(self.main_camera.frame_bottom, self.main_camera.frame_top),
            )))
        self.player = PlayerSprite(image=self.player_image)
        self.add(self.player)

    def hug(self, runner):
        hugged = HuggedSprite(pos=runner.position)
        self.remove(runner)
        self.add(hugged)
        self.add(HeartAnimSprite(anchor=hugged))

    def on_update(self, update, signal):
        bear = next(self.get(kind=PlayerSprite))
        count = 0
        for runner in self.get(kind=RunnerSprite):
            count += 1
            if bear.contains(runner):
                self.hug(runner)

        if not count:
            signal(ppb.events.Quit())


class CharacterSelectSprite(CircularRegion, MenuSprite):
    pass


class CharacterSelectScene(CircularMenuScene):
    ring_increment = 1.25
    item_size = 1.25

    characters = [
        'puffer_fish',
        'owl',
        'coyote',
        'fox',
        'hyena',
        'jackal',
        'wolf',
        'troll',
        'oni',
        'goblin',
        'dark_elf',
        'minotaur',
        'orc',
        'bugbear',
        'demon',
        'elf',
        'kobold',
        'half_demon',
        'fish_person',
        'tiger',
        'cheetah',
        'lion_with_mane',
        'lion_without_mane',
        'jaguar',
        'leopard',
        'lynx',
        'snow_leopard',
        'slime',
        'deer_without_antlers',
        'bear',
        'raccoon',
        'mouse',
        'ram',
        'opossum',
        'rat',
        'rabbit',
        'red_panda',
        'panda',
        'deer_with_antlers',
        'otter',
        'snake',
        'frankensteins_monster',
        'clown',
        'tengu_mask',
        'robot',
        'alien_monster',
        'alien',
    ]

    def get_options(self):
        for e in self.characters:
            yield CharacterSelectSprite(image=Emoji(e))

    def do_select(self, sprite, signal):
        signal(ReplaceScene(
            MainScene(player_image=Emoji(sprite.image.shortcode))
        ))


if __name__ == '__main__':
    ppb.run(
        starting_scene=CharacterSelectScene,
        # resolution=(700, 700),
        # window_title='Hug the Humans!',
    )
