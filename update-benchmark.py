#!/usr/bin/env python3
"""
Benchmark the stability of on_update
"""
import ppb
import time
import statistics as stats


class BenchmarkScene(ppb.BaseScene):
    deltas = []
    perf_deltas = []
    _last_perf = None

    def __init__(self, *p, **kw):
        super().__init__(*p, **kw)
        self._start_time = time.monotonic()

    def on_update(self, update, signal):
        self.deltas.append(update.time_delta)

        if self._last_perf is None:
            self._last_perf = time.perf_counter()
        else:
            t = time.perf_counter()
            self.perf_deltas.append(t - self._last_perf)
            self._last_perf = t

        if (time.monotonic() - self._start_time) > 5:
            signal(ppb.events.Quit())


def analyze(data):
    num = len(data)
    low = min(data)
    hgh = max(data)
    avg = stats.mean(data)
    dev = stats.stdev(data)
    print(f"{low}->{avg}->{hgh}, {dev} (n={num})")


if __name__ == '__main__':
    ppb.run(starting_scene=BenchmarkScene)
    print("Reported")
    analyze(BenchmarkScene.deltas)
    print("Measured")
    analyze(BenchmarkScene.perf_deltas)
