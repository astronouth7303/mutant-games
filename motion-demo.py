#!/usr/bin/env python3
import ppb
from ppb_mutant import Emoji
from hugs import MotionMixin


class DemoSprite(MotionMixin, ppb.Sprite):
    image = Emoji('aerial_tramway')
    max_speed = 1.5

    position = ppb.Vector(-2, 0)
    target = ppb.Vector(+2, 0)

    def on_update(self, update, signal):
        op = self.position
        super().on_update(update, signal)
        np = self.position

        print(
            update.time_delta,
            (np - op),
            ((np - op) * (1 / update.time_delta)).length
        )


class DemoScene(ppb.BaseScene):
    background_color = (0, 100, 0)

    def __init__(self, *p, **kw):
        super().__init__(*p, **kw)

        self.add(DemoSprite())


if __name__ == '__main__':
    ppb.run(starting_scene=DemoScene)
