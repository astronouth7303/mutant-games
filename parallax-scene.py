#!/usr/bin/env python3
"""
Test a parallax menu, where the scene moves under the mouse to extend its reach.
"""
import ppb
from ppb_mutant import Emoji
from utils import (
    CircularRegion, MenuScene
)


class FaceSprite(CircularRegion, ppb.Sprite):
    normal_face = Emoji('smile')
    hover_face = Emoji('big_smile')

    image = normal_face

    def on_mouse_motion(self, mouse, signal):
        if self.contains(mouse.position):
            self.emoji = self.hover_face
        else:
            self.emoji = self.normal_face


class OriginFaceSprite(FaceSprite):
    normal_face = Emoji('cat_smile')
    hover_face = Emoji('cat_grin')

    image = normal_face


class ParallaxScene(MenuScene):
    def __init__(self, *p, **kw):
        super().__init__(*p, pixel_ratio=64, **kw)

        self.xmin = min(s.left for s in self)
        self.xmax = max(s.right for s in self)
        self.ymin = min(s.top for s in self)
        self.ymax = max(s.bottom for s in self)

        self.main_camera.position = ppb.Vector(
            (self.xmin + self.xmax) / 2,
            (self.ymin + self.ymax) / 2,
        )

    def get_options(self):
        for x in range(-20, 21, 5):
            for y in range(-20, 21, 5):
                if x == 0 and y == 0:
                    yield OriginFaceSprite(pos=(x, y))
                else:
                    yield FaceSprite(pos=(x, y))

    def on_pre_render(self, event, signal):
        import time
        time.sleep(0.01)

    def on_mouse_motion(self, mouse, signal):
        x, y = mouse.position
        cam = self.main_camera

        if not (cam.frame_width and cam.frame_height):
            return

        frame_left, frame_right, frame_top, frame_bottom = \
            cam.frame_left, cam.frame_right, cam.frame_top, cam.frame_bottom

        frame_width, frame_height = cam.frame_width, cam.frame_height

        # if not (self.xmin <= x <= self.xmax and self.ymin <= y <= self.ymax):
        #     return

        xpercent = (x - frame_left) / frame_width
        ypercent = (y - frame_bottom) / frame_height

        if xpercent < 0: xpercent = 0
        if xpercent > 1: xpercent = 1
        if ypercent < 0: ypercent = 0
        if ypercent > 1: ypercent = 1

        cam.position = ppb.Vector(
            (self.xmax - self.xmin) * xpercent + self.xmin,
            (self.ymax - self.ymin) * ypercent + self.ymin,
        )


if __name__ == '__main__':
    ppb.run(
        starting_scene=ParallaxScene,
        resolution=(600, 600),
        title='Parallax Test',
    )
