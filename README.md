Some quick games.

Instructions
============

0. Use Python3 (Maybe a venv?)
1. `pip install -r requirements.txt`
2. Run your choice of game:
   * `hugs.py`: Be a monster and hug people
   * `tic-tac-toe.py`: Tic Tac Toe
