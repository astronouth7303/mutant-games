#!/usr/bin/env python3
"""
Play tic-tac-toe, between sun and moon
"""
import ppb
from ppb_mutant import Emoji
from utils import (
    RectangularRegion, MenuScene, MenuSprite,
)


class Player(ppb.flags.Flag, abstract=True): pass


class Cats(Player):
    """
    Exes, cats, moon, etc.
    """


class Dogs(Player):
    """
    Ohs, dogs, sun, etc
    """


class PlayerSprite(ppb.Sprite):
    SYMBOLS = {
        None: ppb.flags.DoNotRender,
        Cats: Emoji('full_moon'),
        Dogs: Emoji('sun'),
    }

    state = None

    @property
    def image(self):
        return self.SYMBOLS.get(self.state, ppb.flags.DoNotRender)


class SquareSprite(PlayerSprite, RectangularRegion, MenuSprite):
    SYMBOLS = {
        None: Emoji('large_white_square'),
        Cats: Emoji('full_moon'),
        Dogs: Emoji('sun'),
    }


class WinSprite(ppb.Sprite):
    image = Emoji('tada')

    state = 'win'


class GameScene(MenuScene):
    resolution = 400, 400

    def __init__(self, *p, **kw):
        super().__init__(*p, pixel_ratio=64, **kw)

        self.current_player = Cats

    @property
    def current_player(self):
        if self.indicator is not None:
            return self.indicator.state

    @current_player.setter
    def current_player(self, value):
        if self.indicator is not None:
            self.indicator.state = value
        else:
            print("WARNING: No indicator, state lost")

    def get_options(self):
        cam = self.main_camera

        self.indicator = PlayerSprite(pos=(0, cam.frame_top - 0.5))
        yield self.indicator

        self.cells = [[None] * 3 for x in range(0, 3)]
        for ix, x in enumerate((-1.1, 0, 1.1)):
            for iy, y in enumerate((-1.1, 0, 1.1)):
                self.cells[ix][iy] = SquareSprite(pos=(x, y))
                yield self.cells[ix][iy]

    def do_select(self, sprite, signal):
        if self.current_player == 'win':
            return
        if sprite.state is None:
            sprite.state = self.current_player
            if self.current_player is Cats:
                self.current_player = Dogs
            else:
                self.current_player = Cats

        if self._check_for_win():
            self.remove(self.indicator)
            self.indicator = WinSprite(pos=self.indicator.position)
            self.add(self.indicator)

    def _check_for_win(self):
        c = self.cells
        # Rows
        for x in range(0, 3):
            if self._check_group(*c[x]):
                return True

        # Columns
        for y in range(0, 3):
            if self._check_group(c[0][y], c[1][y], c[2][y]):
                return True

        # Diagonals
        if self._check_group(c[0][0], c[1][1], c[2][2]):
            return True
        if self._check_group(c[0][2], c[1][1], c[2][0]):
            return True

    def _check_group(self, a, b, c):
        a, b, c = a.state, b.state, c.state
        if None in (a, b, c):
            return False
        else:
            return a == b == c


if __name__ == '__main__':
    ppb.run(
        starting_scene=GameScene,
        resolution=(600, 600),
        title='Tic Tac Toe',
    )
