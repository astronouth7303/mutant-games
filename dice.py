#!/usr/bin/env python
import random
import time

import ppb
from ppb_mutant import Emoji

from utils import MenuSprite, MenuScene, CircularRegion, frange

DIGITS = [
    Emoji(f'{n}_char')
    for n in range(0, 10)
]

DICE = {
    n: Emoji(f'd{n}')
    for n in (4, 6, 8, 10, 12, 20)
}


class Die(CircularRegion, MenuSprite):
    def __init__(self, faces, **props):
        super().__init__(**props)
        self.faces = faces
        self.image = DICE[faces]

    def on_pre_render(self, event, signal):
        scene = event.scene
        if scene.is_rolling and scene.current_die == self.faces:
            self.rotation = random.uniform(0, 360)
        else:
            self.rotation = 0


class RollingScene(MenuScene):
    ROLL_TIME = 1

    roll_start = None

    current_die = None

    @property
    def is_rolling(self):
        return self.roll_start is not None

    def get_options(self):
        # left, right = self.main_camera.left, self.main_camera.right
        # PPB defaults
        left, right = -6.25, +6.25

        gap = (right - left) / (len(DICE) + 1)

        for faces, x in zip(DICE.keys(), frange(left + gap, right, gap)):
            yield Die(faces=faces, position=ppb.Vector(x, 0))

        self.tens = ppb.Sprite(image=ppb.flags.DoNotRender, position=ppb.Vector(-0.4, -1.5))
        self.ones = ppb.Sprite(image=ppb.flags.DoNotRender, position=ppb.Vector(+0.4, -1.5))
        yield self.tens
        yield self.ones

    def do_select(self, sprite, signal):
        self.current_die = sprite.faces
        self.roll_start = time.monotonic()

    def on_update(self, event, signal):
        if self.is_rolling:
            now = time.monotonic()
            if now - self.roll_start > self.ROLL_TIME:
                self.roll_start = None

    def on_pre_render(self, event, signal):
        if self.is_rolling:
            num = random.randint(1, self.current_die)
            self.tens.image = DIGITS[num // 10]
            self.ones.image = DIGITS[num % 10]


if __name__ == '__main__':
    ppb.run(starting_scene=RollingScene)
