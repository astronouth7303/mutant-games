#!/usr/bin/env python3
"""
Show a grid of emoji, aligned.
"""
import ppb
from ppb_mutant import Emoji
from utils import (
    CircularRegion, MenuScene
)


class FaceSprite(CircularRegion):
    normal_face = Emoji('smile')
    hover_face = Emoji('big_smile')

    image = normal_face

    def on_mouse_motion(self, mouse, signal):
        if self.contains(mouse.position):
            self.image = self.hover_face
        else:
            self.image = self.normal_face

    def on_button_pressed(self, mouse, signal):
        if self.contains(mouse.position):
            mouse.scene.main_camera.position = self.position


class OriginFaceSprite(FaceSprite):
    normal_face = Emoji('cat_smile')
    hover_face = Emoji('cat_grin')

    image = normal_face


class GridScene(MenuScene):
    def __init__(self, *p, **kw):
        super().__init__(*p, pixel_ratio=64, **kw)

    def get_options(self):
        cam = self.main_camera
        print(f"({cam.frame_left}, {cam.frame_bottom}) -> ({cam.frame_right}, {cam.frame_top})")
        for x in range(int(cam.frame_left) - 1, int(cam.frame_right) + 1):
            for y in range(int(cam.frame_bottom) - 1, int(cam.frame_top) + 1):
                if (x, y) in [(0, 0), (1, 0), (0, 1)]:
                    yield OriginFaceSprite(pos=(x, y))
                else:
                    yield FaceSprite(pos=(x, y))


if __name__ == '__main__':
    ppb.run(
        starting_scene=GridScene,
        resolution=(600, 600),
        title='Grid Test',
    )
