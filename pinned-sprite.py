#!/usr/bin/env python3
import ppb
from ppb_mutant import Emoji


class OpenMenuSprite(ppb.Sprite):
    image = Emoji('sun')

    def on_pre_render(self, event, signal):
        cam = event.scene.main_camera
        self.position = ppb.Vector(cam.frame_left + 0.5, cam.frame_top - 0.5)


class OriginFaceSprite(ppb.Sprite):
    image = Emoji('full_moon')


class PinnedScene(ppb.BaseScene):
    angular_rate = 360 / 10

    def __init__(self, *p, **kw):
        super().__init__(*p, **kw)

        self.add(OpenMenuSprite())
        self.add(OriginFaceSprite())
        self.main_camera.position = ppb.Vector(0, 2)

    def on_update(self, update, signal):
        cam = self.main_camera
        cam.position = cam.position.rotate(self.angular_rate * update.time_delta)


if __name__ == '__main__':
    ppb.run(
        starting_scene=PinnedScene,
        title='Pinned Sprite',
    )
